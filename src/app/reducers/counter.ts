import { createAction, createFeatureSelector, createReducer, on } from "@ngrx/store";

export const increase = createAction("[COUNTER] increase");
export const decrease = createAction("[COUNTER] decrease");
export const clear = createAction("[COUNTER] clear");

export interface CounterState{
    count: number;
}

export const initState: CounterState = {
    count: 0
}

export const counterReducer = createReducer(
    initState,
    on(increase, state => ({
        ...state,
        count: state.count + 1
    })),
    on(decrease, state => ({
        ...state,
        count: state.count - 1
    })),
    on(clear, state => ({
        ...state,
        count: 0
    }))
);

export const featureSelector = createFeatureSelector<CounterState>("counter");