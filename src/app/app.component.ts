import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { decrease, increase, clear } from './reducers/counter';

@Component({
  selector: 'app-root',
  templateUrl: "./app.component.html",
  styleUrl: "./app.component.css"
})

export class AppComponent {
  counter = 0;
  updatedAt?: number;

  constructor(private store: Store){

  }

  get cannotDecrease(): boolean{
    return this.counter <= 0;
  }  

  increase(): void {
    this.updateDate();
    this.counter++;
    this.store.dispatch(increase());
  }

  decrease(): void {
    this.updateDate();
    this.counter--;
    this.store.dispatch(decrease());
  }

  clear(): void {
    this.updateDate();
    this.counter = 0;
    this.store.dispatch(clear());
  }

  updateDate(): void{
    this.updatedAt = Date.now();
  }
}
